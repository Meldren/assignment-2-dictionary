%define NEXT_NODE 0
%macro colon 2
	%ifid %2
		%2: dq NEXT_NODE
		%define NEXT_NODE %2
	%else
		%error "Expected label as the second argument."
	%endif

	%ifstr %1
		db %1, 0
	%else
		%error "Expected string as the first argument."
	%endif
%endmacro
