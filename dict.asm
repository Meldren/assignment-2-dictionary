%include "lib.inc"
%define QUAD_WORD_SIZE 8

section .text

global find_word

; Принимает указатель на нуль-терминированную строку
; и указатель на начало словаря. Возвращает адрес
; начала найденного элемента или 0 в случае его отсутствия
find_word:
	xor rax, rax
	mov r8, rsi
	.loop:
		add r8, QUAD_WORD_SIZE
		mov rsi, r8
		push r8
		call string_equals
		pop r8
		cmp rax, 1
		je .success
		mov r8, [r8 - QUAD_WORD_SIZE]
		cmp r8, 0
		jne .loop
		ret
	.success:
		mov rax, r8
		ret
