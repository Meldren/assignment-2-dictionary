ASM=nasm
ASM_ARGS=-felf64 -o
LD=ld

.PHONY: all
all: program clean

%.o: %.asm
	$(ASM) $(ASM_ARGS) $@ $<
program: main.o dict.o lib.o
	$(LD) -o $@ $^

.PHONY: clean
clean:
	rm *.o
